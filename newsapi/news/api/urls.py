from django.urls import path
from news.api.views import (
    journalist_list_create_api_view,
    article_list_create_api_view,
    journalist_single_view,
    article_single_view)

urlpatterns = [
    path(
        "journalist/",
        journalist_list_create_api_view,
        name="journalist-list"
    ),
    path(
        "journalist/<int:pk>/",
        journalist_single_view,
        name="journalist-single"
    ),
    path(
        "article/",
        article_list_create_api_view,
        name="article-list"
    ),
    path(
        "article/<int:pk>/",
        article_single_view,
        name="article-single"
    ),
]
