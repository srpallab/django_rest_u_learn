from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from news.models import Article, Journalist
from news.api.serializer import ArticleSerializer, JournalistSerializer


@api_view(['GET', 'POST'])
def journalist_list_create_api_view(request):
    if request.method == 'GET':
        journalist = Journalist.objects.all()
        serializer = JournalistSerializer(
            journalist,
            many=True
        )
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = JournalistSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                serializer.data,
                status=status.HTTP_400_BAD_REQUEST
            )


@api_view(['GET'])
def journalist_single_view(request, pk):
    if request.method == 'GET':
        journalist = Journalist.objects.get(pk=pk)
        serializer = JournalistSerializer(journalist)
        return Response(serializer.data)


@api_view(['GET', 'POST'])
def article_list_create_api_view(request):
    if request.method == 'GET':
        query = Article.objects.all()
        article = ArticleSerializer(
            query,
            many=True
        )
        return Response(article.data)
    if request.method == 'POST':
        serializer = ArticleSerializer(data=request.data)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                serializer.data,
                status=status.HTTP_400_BAD_REQUEST
            )


@api_view(['GET'])
def article_single_view(request, pk):
    if request.method == 'GET':
        article = Article.objects.get(pk=pk)
        serializer = ArticleSerializer(article)
        return Response(serializer.data)
