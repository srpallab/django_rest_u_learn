from rest_framework import serializers
from news.models import Article, Journalist


class ArticleSerializer(serializers.Serializer):
    """ Documentation for ArticleSerializer """
    id = serializers.IntegerField(read_only=True)
    # author = serializers.HyperlinkedRelatedField(
    # many=True
    # read_only=True,
    # view_name="journalist-single"
    # )
    # author = serializers.CharField()
    title = serializers.CharField()
    author = serializers.PrimaryKeyRelatedField(
        read_only=True,
        required=False
    )
    description = serializers.CharField()
    body = serializers.CharField()
    location = serializers.CharField()
    publication_date = serializers.DateField()
    active = serializers.BooleanField()
    created_at = serializers.DateTimeField()
    updated_at = serializers.DateTimeField()

    def create(self, validated_data):
        print(validated_data)
        return Article.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get(
            'title',
            instance.title
        )
        instance.description = validated_data.get()
        instance.body = validated_data.get()
        instance.location = validated_data.get()
        instance.publication_date = validated_data.get()
        instance.active = validated_data.get()
        instance.save()
        return instance


class JournalistSerializer(serializers.Serializer):
    """ Documentation for JournalistSerializer """
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    location = serializers.CharField()
    biography = serializers.CharField()
    article = ArticleSerializer(many=True, read_only=True)

    def create(self, validated_data):
        print(validated_data)
        return Journalist.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get(
            "first_name",
            instance.first_name)
        instance.last_name = validated_data.get(
            "last_name",
            instance.last_name
        )
        instance.location = validated_data.get(
            "location",
            instance.location
        )
        instance.biography = validated_data.get(
            "biography",
            instance.biography
        )
        instance.save()
        return instance
